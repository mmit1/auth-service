module gitlab.com/Burunduck/user-service

go 1.15

//add v0.3.0
require (
	github.com/joho/godotenv v1.3.0
	github.com/spf13/jwalterweatherman v1.1.0
	github.com/spf13/viper v1.7.1
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.25.0
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.8
)
