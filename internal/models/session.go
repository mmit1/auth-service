package models

import (
	"context"
	"gorm.io/gorm"
)

type Session struct {
	gorm.Model
	ID     string `gorm:"primaryKey;autoIncrement:false"`
	UserID int
}

type SessionRepository interface {
	Create(userId int) (string, error)
	GetUserId(session string) (int, error)
	DeleteSession(ctx context.Context, session string) error
}
