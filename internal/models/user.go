package models

import (
	"context"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID          int    `gorm:"primaryKey;autoIncrement:true;"`
	Login       string
	Password    string
	OldPassword string
	FirstName   string
	LastName    string
}

type UserUsecase interface {
	Register(ctx context.Context, u *User) error
	Login(ctx context.Context, u *User) (string, error)
	GetUserBySession(ctx context.Context, session string) (*User, error)
	Logout(ctx context.Context, session string) error
}

type UserRepository interface {
	Register(ctx context.Context, u *User) error
	GetUserByCreds(ctx context.Context,  u *User) (int , error)
	GetUserById(ctx context.Context, id int) (*User, error)
}
