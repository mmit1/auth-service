package usecase

import (
	"context"
	"gitlab.com/Burunduck/user-service/internal/models"
	"time"
)

type UserUsecase struct {
	userRepo       models.UserRepository
	sessionRepo    models.SessionRepository
	contextTimeout time.Duration
}

func NewUserUsecase(u models.UserRepository, s models.SessionRepository, t time.Duration) models.UserUsecase {
	return &UserUsecase{
		userRepo:       u,
		sessionRepo:    s,
		contextTimeout: t,
	}
}

func (uu *UserUsecase) Register(c context.Context, u *models.User) error {
	ctx, cancel := context.WithTimeout(c, uu.contextTimeout)
	defer cancel()
	err := uu.userRepo.Register(ctx, u)
	return err
}

func (uu *UserUsecase) Login(c context.Context, u *models.User) (string, error) {
	ctx, cancel := context.WithTimeout(c, uu.contextTimeout)
	defer cancel()

	id, err := uu.userRepo.GetUserByCreds(ctx, u)
	if err != nil {
		return "", err
	}

	session, err := uu.sessionRepo.Create(id)
	if err != nil {
		return "", err
	}
	return session, nil
}

func (uu *UserUsecase) GetUserBySession(ctx context.Context, session string) (*models.User, error) {
	userId, err := uu.sessionRepo.GetUserId(session)
	if err != nil {
		return nil, err
	}
	user, err := uu.userRepo.GetUserById(ctx, userId)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (uu *UserUsecase) Logout(ctx context.Context, session string) error {
	err := uu.sessionRepo.DeleteSession(ctx, session)
	if err != nil {
		return err
	}
	return nil
}
